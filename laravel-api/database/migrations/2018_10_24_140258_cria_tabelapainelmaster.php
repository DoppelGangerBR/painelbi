<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelapainelmaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_painel', function (Blueprint $table) {
            $table->integer('codempresa')->nullable();
            $table->string('nomeempresa')->nullable();
            $table->string('logoempresa')->nullable();
            $table->integer('codpaineloferta')->nullable();
            $table->string('tipooferta')->nullable();
            $table->string('nometipooferta')->nullable();
            $table->integer('codcliente')->nullable();
            $table->integer('codfornecedor')->nullable();
            $table->integer('codlocalarmazenagem')->nullable();
            $table->integer('codmunicipio')->nullable();
            $table->integer('codfuncionario')->nullable();
            $table->integer('codproduto')->nullable();
            $table->integer('quantidade')->nullable();
            $table->integer('quantidadesacas')->nullable();
            $table->double('valorunitario')->nullable();
            $table->double('valortotal')->nullable();
            $table->string('nome')->nullable();
            $table->string('nomemunicipio')->nullable();
            $table->string('localarmazenagem')->nullable();
            $table->string('observacao')->nullable();
            $table->string('nomefuncionario')->nullable();
            $table->string('nomeproduto')->nullable();
            $table->string('nomestatus')->nullable();
            $table->string('status')->nullable();
            $table->string('tipoproduto')->nullable();
            $table->date('data')->nullable();
            $table->date('datasincronizacao')->nullable();
            $table->date('dataretirada')->nullable();
            $table->string('codCor')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_painel')->nullable();
    }
}
