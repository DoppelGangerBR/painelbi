<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterPainel;
use Illuminate\Support\Facades\DB;
class MasterController extends Controller
{
    //
    public function index(){
        //$dados = MasterPainel::all();
        $dados = DB::table('master_painel')->orderBy('dataretirada','asc')->get();
        return response()->json($dados);
    }
    public function retornaDistinto(){
        $produtos = DB::table('master_painel')->groupBy('nomeproduto')->pluck('nomeproduto');
        $produtor = DB::table('master_painel')->groupBy('nome')->pluck('nome');
        $armazem = DB::table('master_painel')->groupBy('localarmazenagem')->pluck('localarmazenagem');
        $local = DB::table('master_painel')->groupBy('nomemunicipio')->pluck('nomemunicipio');
        $comercial = DB::table('master_painel')->groupBy('nomefuncionario')->pluck('nomefuncionario');

        return response()->json(['produtos' => $produtos,'produtores' => $produtor,'armazens' =>$armazem,'locais' => $local,'comerciais'=>$comercial]);

    }
}
