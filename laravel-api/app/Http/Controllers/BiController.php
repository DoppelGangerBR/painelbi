<?php

namespace App\Http\Controllers;
use App\Bi;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BiController extends Controller
{
    protected $tables = 'bi';
    public function index(){ //Lista todos os Bis
        $bis = Bi::where('ativo',true)->get();
        return response()->json($bis);
    }
    public function store(Request $request){
        $dados = $request->all();
        try{
            $Bi = Bi::create($dados);
            if($Bi){
                return response()->json([$Bi, 'status' => true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false, $e]);           
        }
    }
    public function RetornaBis(Request $request){ //Lista Bis de um determinado usuario
        $usuario;
        $dados = $request->only(['token']);  
        try{
            //$dados = $request->all();
            $usuario = DB::table('usuario')->where('auth_token', '=', $dados['token'])->pluck('codusuario');
            //$Usuario = Usuario::where('auth_token', $dados);
            if($usuario){
                $bis = DB::table('bi')->where('codusuario', '=', $usuario)->where('ativo', '=', true)->get();
                //$bis = Bi::where('codusuario', $Usuario['codusuario']);
                if($bis){
                    return response()->json($bis);
                }else{
                    return response()->json(["msg"=>'Nenhum B.I cadastrado']);
                }            
            }else{
                return response()->json(['msg' => 'Usuario não encontrado ']);
            }
            //return response()->json([$bis]);            
        }catch(\Exception $e){
            return response()->json(['Erro' => 'Erro ao acessar a base de dados\nDescrição do erro: '+$e]);
            //return response()->json(['erro' => $e]);
        }
        
    }
    public function update(Request $request, $codbi){
        $dadosAlteracao = $request->all();
        $status = false;
        $Bi = Bi::where('codbi', '=', $codbi)->first();
        try{
            $update = $Bi->update($dadosAlteracao);
            if($update){
                $status = true;
            }            
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }
    public function desativaPainel(Request $request){
        $status = false;
        $dados = $request->all();
        $codbi = $dados['codbi'];
        try{
            $Bi = Bi::where('codbi', '=', $codbi);
            $desativa = $Bi->update($dados);
            if($desativa)            {
                $status = true;
            }
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }
}
