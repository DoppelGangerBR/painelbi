<?php

namespace App\Http\Controllers;
use App\Usuario;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use JWTGuard;
class UsuarioController extends Controller
{
    public function index()
    {
        $usuario = Usuario::where('ativo', true)->get();
        return response()->json($usuario);
    }
    public function store(Request $request){
        $dados = $request->all();
        $dados['senha'] = \Hash::make($dados['senha']); //criptografa a senha
        try{
            $Usuario = Usuario::create($dados);
            if($Usuario){
                return response()->json([$Usuario, 'status'=>true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }catch(\Exception $e){
            return response()->json(['status' => false ,$e]);
        }
        
    }

    public function update(Request $request, $codusuario){
        $dadosAlteracao = $request->all();
        if(array_key_exists('senha', $dadosAlteracao)){
            if($dadosAlteracao['senha'] != null || $dadosAlteracao['senha'] != ''){
                $dadosAlteracao['senha']  = \Hash::make($dadosAlteracao['senha']);
            }        
        }
        
        $Usuario = Usuario::where('codusuario' ,'=', $codusuario)->first();
        try{
            $update = $Usuario->update($dadosAlteracao);
            if($update){
                return response()->json(['status'=>true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false, $e]);
        }
    }
    public function desativaUsuario(Request $request){
        $status = false;
        $dados = $request->all();
        $codusuario = $dados['codusuario'];
        try{
            $Usuario = Usuario::where('codusuario', '=', $codusuario);
            $desativa = $Usuario->update($dados);
            if($desativa)            {
                $status = true;
            }
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }

    public function guard()
    {
        return Auth::guard();
    }
}