<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterPainel extends Model
{
    protected $fillable  = ['codempresa', 'nomeempresa','codpaineloferta', 'tipooferta', 'nometipooferta', 'dataretirada', 'codcliente', 'codfornecedor', 'nome', 'codlocalarmazenagem', 'localarmazenagem', 'observacao', 'codmunicipio', 'nomemunicipio', 'codfuncionario', 'nomefuncionario', 'codproduto', 'nomeproduto', 'tipoproduto', 'quantidade', 'quantidadesacas', 'valorunitario', 'valortotal', 'status', 'nomestatus', 'data', 'datasincronizacao','codcor'];    
    protected $table = 'master_painel';

    public function masterpainel(){
        return $this->hasMany('App\MasterPainel');
    }
}
