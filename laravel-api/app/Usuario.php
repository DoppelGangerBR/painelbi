<?php

namespace App;

/*use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use JWTAuth;
use JWTGuard;
use Tymon\JWTAuth\Contracts\JWTSubject;*/

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Usuario extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable  = ['nome','email','senha','superuser','ativo'];
    protected $hidden = ['senha','created_at','updated_at','deleted_at','ativo','id','update_at','auth_token'];
    protected $datas = ['deleted_at'];
    protected $table = 'usuario';

    public function usuario(){
        return $this->hasMany('App\Usuario');
    }
    public function getJWTIdentifier(){
        return $this->getKey();
    }
    public function getJWTCustomClaims(){
        return [];
    }
}
