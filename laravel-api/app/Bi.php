<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bi extends Model
{
    protected $fillable  = ['codusuario','url','nomebi','ativo'];
    protected $table = 'bi';
    
    function bis(){
        return $this->belongsTo('App\Bis');
    }
}
