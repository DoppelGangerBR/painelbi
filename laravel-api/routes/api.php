<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::get('/', function(){
        return response()->json(['message' => 'Usuarios API', 'Status' => 'Connected']);;
    });
    Route::post('Usuario', 'UsuarioController@store');
   
    Route::post('auth/login','AutorizacaoController@login');
    Route::post('auth/logout','AutorizacaoController@logout');
    Route::post('auth', 'AutorizacaoController@ValidaToken');

    Route::group(['middleware' => ['jwt.verify']], function(){
        Route::get('principal', 'AutorizacaoController@closed');
        Route::post('bis', 'BiController@RetornaBis');
        Route::post('authadm', 'AutorizacaoController@VerificaSeEAdministrador');
        Route::put('usuario/desativa/', 'UsuarioController@desativaUsuario');
        Route::put('Bi/desativa/', 'BiController@desativaPainel');
        Route::resource('usuario', 'UsuarioController');
        Route::resource('Bi', 'BiController');
        Route::get('filtros','MasterController@retornaDistinto');
        Route::get('master','MasterController@index');
    });
    
    

Route::get('/', function(){
    return redirect('api');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
