function PopulaPainel(){
    var request = new XMLHttpRequest();
    var Token = new Object();
    var tokenCookie = document.cookie;    
    Token.token = tokenCookie.replace('tk=','');
    var corpoTabela = document.getElementById("corpoTabela"); 
    request.open('GET', 'http://192.168.0.53:8089/api/master');
    request.setRequestHeader("Content-type", "application/json");
    request.setRequestHeader("Authorization", "bearer "+Token.token);
    request.responseType = "text";
    request.send();
    request.onreadystatechange = function(){
        try{
            if(request.readyState == XMLHttpRequest.DONE){
                if(request.status == 200){
                    dadosPainel = JSON.parse(request.response);                    
                    try{
                        for(var linha in dadosPainel){ 
                            //Cria o elemento de nova linha na tabela                            
                            var LinhaTabela = document.createElement("tr");
                            //Insere a data de retirada
                            var dataretirada = document.createElement("td");
                            dataretirada.className = "colunaRetiradaPainelMaster";
                            var dataFormatada = dadosPainel[linha].dataretirada;
                            dataFormatada = dataFormatada.replace('-','/');
                            dataFormatada = dataFormatada.replace('-','/');
                            var data = new Date(dataFormatada);                            
                            var dia = data.getDate();
                            var mes = data.getMonth() +1;
                            var ano = data.getFullYear(); 
                            if(dia.toString().length == 1){
                                dia = '0'+dia;
                            }
                            if(mes.toString().length == 1){
                                mes = '0'+mes;
                            }
                            data = dia+'/'+mes+'/'+ano;
                            var texto = document.createTextNode(data);
                            dataretirada.appendChild(texto);
                            //Insere nome do produto
                            var nomeproduto = document.createElement("td");
                            var texto = document.createTextNode(dadosPainel[linha].nomeproduto);
                            nomeproduto.appendChild(texto);               
                            //Insere tipo do produto
                            var tipoproduto = document.createElement("td");
                            var texto = document.createTextNode(dadosPainel[linha].tipoproduto);
                            tipoproduto.appendChild(texto);   
                            //Insere nome do produtor
                            var nome = document.createElement("td");
                            var texto = document.createTextNode(dadosPainel[linha].nome);
                            nome.appendChild(texto);
                            //Insere armazem
                            var localarmazenagem = document.createElement("td");
                            var texto = document.createTextNode(dadosPainel[linha].localarmazenagem);
                            localarmazenagem.appendChild(texto);
                            //Insere local
                            var nomemunicipio = document.createElement("td");
                            var texto = document.createTextNode(dadosPainel[linha].nomemunicipio);
                            nomemunicipio.appendChild(texto);
                            //Insere comercial
                            var nomefuncionario = document.createElement("td");       
                            var texto = document.createTextNode(dadosPainel[linha].nomefuncionario);
                            nomefuncionario.appendChild(texto);
                            //Insere quantidades
                            //Documentação da biblioteca numerals -> http://numeraljs.com/
                            var quantidade = document.createElement("td");
                            var texto = document.createTextNode(numeral(dadosPainel[linha].quantidade).format('0,0,0'));                          
                            quantidade.appendChild(texto);
                            
                            //Insere quantidade de sacas
                            var quantidadesacas = document.createElement("td");
                            var texto = document.createTextNode(numeral(dadosPainel[linha].quantidadesacas).format('0,0,0'));
                            quantidadesacas.appendChild(texto);
                            //Insere valor unitario                            
                            var valorunitario = document.createElement("td");
                            valorunitario.id = "vlrUnitario"+linha;
                            var texto = document.createTextNode('R$'+numeral(dadosPainel[linha].valorunitario).format('0,0.00'));
                            valorunitario.appendChild(texto);

                            var corLinha = dadosPainel[linha].codcor;

                            LinhaTabela.appendChild(dataretirada);
                            LinhaTabela.appendChild(nomeproduto);
                            LinhaTabela.appendChild(tipoproduto);
                            LinhaTabela.appendChild(nome);
                            LinhaTabela.appendChild(localarmazenagem);
                            LinhaTabela.appendChild(nomemunicipio);
                            LinhaTabela.appendChild(nomefuncionario);
                            LinhaTabela.appendChild(quantidade);
                            LinhaTabela.appendChild(quantidadesacas);      
                            LinhaTabela.appendChild(valorunitario);
                            LinhaTabela.id = "linha"+linha;
                            
                            LinhaTabela.setAttribute("data-container","body");
                            LinhaTabela.setAttribute("data-toggle","tooltip");
                            LinhaTabela.setAttribute("data-placement","bottom");                            
                            LinhaTabela.setAttribute("tooltip-append-to-body", true);
                            LinhaTabela.obs = dadosPainel[linha].observacao;
                            LinhaTabela.onclick = function (){
                                abreDivObservacao(this.obs)
                            }
                            LinhaTabela.setAttribute("style","background-color:"+corLinha);
                            corpoTabela.appendChild(LinhaTabela);
                        }
                        var dataFormatada = dadosPainel[linha].datasincronizacao;
                            dataFormatada = dataFormatada.replace('-','/');
                            dataFormatada = dataFormatada.replace('-','/');
                            var data = new Date(dataFormatada);
                            
                            var dia = data.getDate();
                            var mes = data.getMonth() +1;
                            var ano = data.getFullYear(); 
                            if(dia.toString().length == 1){
                                dia = '0'+dia;
                            }
                            if(mes.toString().length == 1){
                                mes = '0'+mes;
                            }
                            data = dia+'/'+mes+'/'+ano;
                        var DataSincronizacao = document.getElementById('labelDataSincronizacao');
                        DataSincronizacao.innerHTML = "Sincronizado em.: "+data+ " as "+ dadosPainel[linha].horasincronizacao;
                        
                    }catch(err){
                        console.log('Erro ao inserir dados no dropdown '+err);
                    }
                    
                }else{
                    console.log("Response: "+request.response+"\nCodigo: "+request.status);
                }
            }
        }catch(err){
            console.log('erro detectado = '+err)
        }
    }
}
        
function carregaValoresNosFiltros(){
    var Filtros = new Object();
    filtroProduto = document.getElementById("selectProduto");
    filtroProdutor = document.getElementById("selectProdutor");
    filtroArmazem = document.getElementById("selectArmazem");
    filtroLocal = document.getElementById("selectLocal");
    filtroComercial = document.getElementById("selectComercial");   
    var request = new XMLHttpRequest();
    var Token = document.cookie.replace('tk=','');
    //var tokenCookie = document.cookie;    
    //Token.token = tokenCookie.replace('tk=','');    
    request.open('GET', 'http://192.168.0.53:8089/api/filtros');
    request.setRequestHeader("Content-type", "application/json");
    request.setRequestHeader("Authorization", "bearer "+Token);    
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){
                Filtros = JSON.parse(request.response);                
                for(ftrProduto in Filtros.produtos){
                    var opcaoFiltroProduto = document.createElement("option");        
                    var texto = document.createTextNode(Filtros.produtos[ftrProduto]);
                    opcaoFiltroProduto.appendChild(texto);
                    filtroProduto.appendChild(opcaoFiltroProduto);
                }
                for(ftrProdutor in Filtros.produtores){
                    var opcaoFiltroProdutor = document.createElement("option");
                    var texto = document.createTextNode(Filtros.produtores[ftrProdutor]);
                    opcaoFiltroProdutor.appendChild(texto);
                    filtroProdutor.appendChild(opcaoFiltroProdutor);
                }
                /*for(ftrArmazem in Filtros.armazens){
                    var opcaoFiltroProdutor = document.createElement("option");
                    var texto = document.createTextNode(Filtros.armazens[ftrArmazem]);
                    opcaoFiltroProdutor.appendChild(texto);
                    filtroArmazem.appendChild(opcaoFiltroProdutor);
                }*/
                for(ftrLocais in Filtros.locais){
                    var opcaoFiltroLocal = document.createElement("option");
                    var texto = document.createTextNode(Filtros.locais[ftrLocais]);
                    opcaoFiltroLocal.appendChild(texto);
                    filtroLocal.appendChild(opcaoFiltroLocal);
                }
                for(ftrComercial in Filtros.comerciais){
                    var opcaoFiltroComercial = document.createElement("option");
                    var texto = document.createTextNode(Filtros.comerciais[ftrComercial]);
                    opcaoFiltroComercial.appendChild(texto);
                    filtroComercial.appendChild(opcaoFiltroComercial);
                }
            }
        }
    }
}
    
//Função que carrega a observação na caixa verde de observações
function abreDivObservacao(obs){    
    var divObs = document.getElementById('divObservacao');
    divObs.className = "divObs";
    divObs.innerHTML = obs;
}