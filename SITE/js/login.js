//Cors no Aspnet core https://dzone.com/articles/cors-in-net-core-net-core-security-part-vi
function FazLogin(){    
    var request = new XMLHttpRequest();    
    var UsuarioLogando = new Object();
    var UsuarioLogado = new Object();
    var caixaErro = document.getElementById('divCaixaErro');
    UsuarioLogando.email = document.getElementById("TxtEmail").value;
    UsuarioLogando.senha = document.getElementById("TxtSenha").value;    
    var JsonLogin = JSON.stringify(UsuarioLogando);
    request.open('POST', 'http://192.168.0.53:8089/api/auth/login/',true);    
    request.setRequestHeader("Content-type","application/json");    
    try{
        request.send(JsonLogin);
        request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){
                caixaErro.className = "alert alert-success mensagemErro text-center mt-1 mb-1";
                caixaErro.innerHTML = "Sucesso!";
                caixaErro.role = 'alert';                
                UsuarioLogado = JSON.parse(request.response);                
                delete_cookie('tk');                
                document.cookie += "tk="+UsuarioLogado.token_acesso;                
                window.location.href = "PrincipalBis.html";
            }else{
                var Erro = JSON.parse(request.response);                
                caixaErro.className = "alert alert-danger mensagemErro text-center mt-1 mb-1";
                caixaErro.role = 'alert';
                caixaErro.innerHTML = Erro.Erro;
            }
        }
    }
    }catch(err){
        console.log("Erro: "+err)
    }
}

function delete_cookie( name ) {    
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function KeyPressEnterLogin(event){
    if(event.keyCode == 13){
        FazLogin();
    }
}
function SetaProximoFoco(event,proximoElemento){ 
    if(event.keyCode == 13){
        document.getElementById(proximoElemento).focus();   
    }
}