//https://stackoverflow.com/questions/23901447/dynamic-population-of-dropdown-menu-with-bootstrap
function PopulaDropDown(){
    var request = new XMLHttpRequest();
    var Token = new Object();
    var bis = new Object();
    var tokenCookie = document.cookie;    
    Token.token = tokenCookie.replace('tk=','');
    var dropdownMenu = document.getElementById("divDropDown");    
    var botaoPainelNav = document.getElementById('btnAbrePainel');
    var JsonToken = JSON.stringify(Token);    
    request.open('POST', 'http://192.168.0.53:8089/api/bis');
    request.setRequestHeader("Content-type", "application/json");
    request.responseType = "text";
    request.send(JsonToken);
    request.onreadystatechange = function(){
        try{
            if(request.readyState == XMLHttpRequest.DONE){
                if(request.status == 200){
                    bis = JSON.parse(request.response);             
                    try{
                        for(var qtdbis in bis){                            
                            var urlBis = document.createElement("a");
                            var texto = document.createTextNode(bis[qtdbis].nomebi);
                            urlBis.appendChild(texto);
                            urlBis.url = bis[qtdbis].url;
                            urlBis.setAttribute = bis[qtdbis].url;
                            urlBis.className = "dropdown-item";
                            urlBis.id = bis[qtdbis].nomebi;                            
                            urlBis.onclick = function(){                                
                                abreFrameBi(this.id, this.url);
                            }
                            if(bis[qtdbis].nomebi.toLowerCase() == "painel de ofertas"){
                                botaoPainelNav.className = "exibeMenuAdministrador nav-link";
                                botaoPainelNav.href = "master.html";
                            }
                            if(bis[qtdbis].nomebi.toLowerCase() == "painel de ofertas" && qtdbis == 0){
                                botaoPainelNav.className = "exibeMenuAdministrador nav-link";
                                botaoPainelNav.href = "master.html";
                                window.location.href = "master.html";
                            }
                            dropdownMenu.appendChild(urlBis);                            
                            //console.log('BI Inserido = '+ bis[qtdbis].nomebi);
                            console.log(qtdbis);
                        }    
                    }catch(err){
                        console.log('Erro ao inserir dados no dropdown '+err);
                    }
                    //console.log("Terminou a execução");
                }else{
                    //alert("Erro, status diferente de 200");
                }
            }
        }catch(err){
            console.log('erro detectado = '+err)
        }
    }
    
    
    
}

function VerificaAdministrador(){    
    var Administrador = new Object();
    var Token = new Object();
    var request = new XMLHttpRequest();
    var tokenCookie = document.cookie;    
    Token.token = tokenCookie.replace('tk=','');    
    var JsonToken = JSON.stringify(Token);
    var dropDownMenuAdm = document.getElementById("listaAdmDropDown");
    //Verifica se o usuario é administrador ou não para poder liberar os menus de administrador
    try{
        request.open('POST', 'http://192.168.0.53:8089/api/authadm', true);
        request.setRequestHeader("Content-type", "application/json");        
        request.send(JsonToken);
        request.onreadystatechange = function(){
            if(request.readyState == request.DONE){
                if(request.status == 200){               
                    Administrador = JSON.parse(request.response);
                    if(Administrador[0].superuser){
                        dropDownMenuAdm.className = "nav-item dropdown exibeMenuAdministrador";
                    }else{                        
                        dropDownMenuAdm.className = 'ocultaMenuAdministrador';
                    }
                }
            }
        }
    }catch(err){
        console.log("Deu erro, viiiiix "+err)
    }
}

function abreFrameBi(nomeBi, url){
    console.log(nomeBi);
    try{
        if(nomeBi ==  "Painel de ofertas"){
            window.location.href = "master.html";            
        }else{
            var iframe = document.getElementById('frameBis');
            var tituloBi = document.getElementById('tituloBi');
            
            var textoNome = document.createTextNode(nomeBi);
            if(tituloBi.firstChild != null){
                tituloBi.removeChild(tituloBi.firstChild);                                
            }
            tituloBi.appendChild(textoNome);        
            iframe.src = url;    
            iframe.className = "embed-responsive-item ";
        }
    }catch(err){
        console.log("Erro abreframe "+err)
    }
}
function PopulaSelectUsuario(idSelect){
    var request = new XMLHttpRequest();
    var Token = new Object();
    var Usuarios = new Object();
    var tokenCookie = document.cookie;
    Token.token = tokenCookie.replace('tk=', '');
    var ElementoSelect = document.getElementById(idSelect);
    if(ElementoSelect.childElementCount > 1){
        ElementoSelect.removeChild()
    }
    var JsonToken = JSON.stringify(Token);
    request.open('GET','http://192.168.0.53:8089/api/usuario', true);
    request.setRequestHeader('Content-Type','application/json');
    request.setRequestHeader('Authorization', 'bearer '+Token.token);
    request.send(JsonToken);
    request.onreadystatechange = function(){
        if(request.readyState == request.DONE){
            if(request.status == 200){
                var Usuarios = JSON.parse(request.response);
                for(qtdUsuarios in Usuarios){
                    var option = document.createElement("option");
                    var texto = document.createTextNode(Usuarios[qtdUsuarios].nome);
                    option.value = Usuarios[qtdUsuarios].codusuario;
                    option.appendChild(texto);
                    ElementoSelect.appendChild(option);
                }
            }
        }
    }

}

function PopulaSelectPainel(idSelect){
    var request = new XMLHttpRequest();
    var Token = new Object();
    var Paineis = new Object();
    var tokenCookie = document.cookie;
    Token.token = tokenCookie.replace('tk=', '');
    var ElementoSelect = document.getElementById(idSelect);
    if(ElementoSelect.childElementCount > 1){
        ElementoSelect.removeChild(ElementoSelect.ELEMENT_NODE)
    }
    
    request.open('GET','http://192.168.0.53:8089/api/Bi', true);
    request.setRequestHeader('Content-Type','application/json');
    request.setRequestHeader('Authorization', 'bearer '+Token.token);
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == request.DONE){
            if(request.status == 200){
                var Paineis = JSON.parse(request.response);
                for(qtdPaineis in Paineis){
                    var option = document.createElement("option");
                    var texto = document.createTextNode(Paineis[qtdPaineis].nomebi);
                    option.value = Paineis[qtdPaineis].codbi;
                    option.appendChild(texto);
                    ElementoSelect.appendChild(option);
                }
            }
        }
    }

}