function RegistraUsuario(){
    var Usuario = new Object();
    var request = new XMLHttpRequest();
    var administrador = document.getElementById('checkAdministrador');
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonUsuario;
    Usuario.nome = document.getElementById('txtNome').value;
    Usuario.email = document.getElementById('txtEmail').value;
    Usuario.senha = document.getElementById('txtSenha').value;
    Usuario.superuser = false;
    if(Usuario.senha == null || Usuario.senha == ""){
        divErro.className = "d-flex col-12 justify-content-center";
        divTextoErro.className = "col-6 alert alert-danger text-center";
        divTextoErro.innerHTML = "Por favor, preencha todos os campos";
    }else{
        if(administrador.checked){
            Usuario.superuser = true;
        }
        Usuario.ativo = true;
        JsonUsuario = JSON.stringify(Usuario);
        var token = document.cookie;
        token = token.replace("tk=", "");
        
        request.open('POST', 'http://192.168.0.53:8089/api/usuario', true);
        request.setRequestHeader("Content-Type", "Application/json");
        request.setRequestHeader("Authorization", "bearer "+token);
        try{
            request.send(JsonUsuario);
            request.onreadystatechange = function(){
                if(request.readyState = request.DONE){
                    if(request.status == 200){
                        var NovoUsuario = new Object();
                        NovoUsuario = JSON.parse(request.response);
                        if(NovoUsuario.status != false){
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                            divTextoErro.innerHTML = "Novo usuário registrado com sucesso!";

                        }else{
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                            if(NovoUsuario[0].errorInfo[0] == 23505){
                                divTextoErro.innerHTML = "O email informado já esta em uso!";
                            }
                            if(NovoUsuario[0].errorInfo[0] == 23502){
                                divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                            }
                        }
                    }
                }
            }
        }catch(err){
            console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
        }
        }
    }
    
function AlteraUsuario(){
    var Usuario = new Object();
    var request = new XMLHttpRequest();
    var administrador = document.getElementById('checkAdministradorAlterado');
    var ElementoSelect = document.getElementById('selectAlteracaoUsuario');
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonUsuario;
    var codusuario = ElementoSelect[ElementoSelect.selectedIndex].value;
    if(codusuario != 1000){
        Usuario.nome = document.getElementById('txtNomeAlterado').value;  
        Usuario.email = document.getElementById('txtEmailAlterado').value;
        Usuario.senha = document.getElementById('txtSenhaAlterado').value;
    
        if(document.getElementById('txtNomeAlterado').value == null || document.getElementById('txtNomeAlterado').value == ""){
            Usuario.nome = undefined;
        }
        if(document.getElementById('txtEmailAlterado').value == null || document.getElementById('txtEmailAlterado').value == ""){
            Usuario.email = undefined;
        }
        if(document.getElementById('txtSenhaAlterado').value == null || document.getElementById('txtSenhaAlterado').value == ""){
            Usuario.senha = undefined;
        }    
        Usuario.superuser = false;
        if(administrador.checked){
            Usuario.superuser = true;
        }
        JsonUsuario = JSON.stringify(Usuario);
        var token = document.cookie;
        token = token.replace("tk=", "");    
        request.open('PUT', 'http://192.168.0.53:8089/api/usuario/'+codusuario, true);
        request.setRequestHeader("Content-Type", "Application/json");
        request.setRequestHeader("Authorization", "bearer "+token);
        try{
            request.send(JsonUsuario);
            console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaa"+JsonUsuario);
            request.onreadystatechange = function(){
                if(request.readyState = request.DONE){
                    if(request.status == 200){
                        var UsuarioAlterado = new Object();
                        UsuarioAlterado = JSON.parse(request.response);
                        console.log(UsuarioAlterado);
                        if(UsuarioAlterado.status != false){
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                            divTextoErro.innerHTML = "Novo usuário registrado com sucesso!";
                        }else{
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                            if(UsuarioAlterado[0].errorInfo[0] == 23505){
                                divTextoErro.innerHTML = "O email informado já esta em uso!";
                            }
                            if(UsuarioAlterado[0].errorInfo[0] == 23502){
                                divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                            }
                        }
                    }
                }
            }
        }catch(err){
            console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
        }
    }else{
        divErro.className = "d-flex col-12 justify-content-center";
        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";        
        divTextoErro.innerHTML = "Por favor, selecionar um usuario para alterar!";
    }    
}


function DesativaUsuario(){
    var Usuario = new Object();
    var request = new XMLHttpRequest();
    var ElementoSelect = document.getElementById('selectRemocaoUsuario');
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonUsuario;
    Usuario.codusuario = ElementoSelect[ElementoSelect.selectedIndex].value;
    codusuario = ElementoSelect[ElementoSelect.selectedIndex].value;
    if(codusuario != 1000){
        Usuario.ativo = false;
        JsonUsuario = JSON.stringify(Usuario);
        var token = document.cookie;
        token = token.replace("tk=", "");
        request.open('PUT', 'http://192.168.0.53:8089/api/usuario/desativa', true);
        request.setRequestHeader("Content-Type", "Application/json");
        request.setRequestHeader("Authorization", "bearer "+token);
        try{
            request.send(JsonUsuario);
            request.onreadystatechange = function(){
                if(request.readyState = request.DONE){
                    if(request.status == 200){
                        var UsuarioDesativado = new Object();
                        UsuarioDesativado = JSON.parse(request.response);
                        if(UsuarioDesativado.status != false){
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                            divTextoErro.innerHTML = "Usuario desativado com sucesso!";
    
                        }else{
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";                        
                            divTextoErro.innerHTML = "Erro ao desativar o usuario, por favor, contate o administrador!";
                            
                        }
                    }
                }
            }
        }catch(err){
            console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
        }
    }else{
        divErro.className = "d-flex col-12 justify-content-center";
        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";                        
        divTextoErro.innerHTML = "Por favor, selecione um usuario a ser desativado!";
    }
    
}



