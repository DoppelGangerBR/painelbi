function RegistraPainel(){
    var request = new XMLHttpRequest();
    var Painel = new Object();
    var ElementoSelect = document.getElementById('selectUsuario');   
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonPainel;
    Painel.nomebi = document.getElementById('txtNomePainel').value;
    Painel.url = document.getElementById('txtUrlPainel').value;
    Painel.codusuario = ElementoSelect[ElementoSelect.selectedIndex].value; //Pega o valar do atributo value do select que no caso e o codusuario
    Painel.ativo = true;
    JsonPainel = JSON.stringify(Painel);
    var token = document.cookie;
    token = token.replace('tk=','');
    request.open('POST','http://192.168.0.53:8089/api/Bi', true);
    request.setRequestHeader('Content-type','application/json');
    request.setRequestHeader('Authorization','bearer '+token);
    request.send(JsonPainel);
    request.onreadystatechange = function(){
        if(request.readyState == request.DONE){
            if(request.status == 200){
                var NovoPainel = new Object();
                NovoPainel = JSON.parse(request.response);
                console.log(NovoPainel);
                if(NovoPainel.status != false){
                    divErro.className = "d-flex col-12 justify-content-center";
                    divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                    divTextoErro.innerHTML = "Novo painel registrado com sucesso!";
                }else{
                    divErro.className = "d-flex col-12 justify-content-center";
                    divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";                    
                    if(NovoPainel[0].errorInfo[0] == 23502){
                        divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                    }
                }
            }
        }
    }
}

function AlteraPainel(tipoOperacao, idSelectPainel){
    var request = new XMLHttpRequest();
    var Painel = new Object();
    var codusuario = document.getElementById('selectUsuarioPainelAlteracao');   
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonPainel;
    var token = document.cookie.replace('tk=','');    
    codbi = document.getElementById(idSelectPainel);
    Painel.codbi = codbi[codbi.selectedIndex].value
    if(tipoOperacao == 1){ //Alteracao
        if(Painel.codbi != 1000){
            Painel.nomebi = document.getElementById('txtNomePainelAlteracao').value;
            if(Painel.nomebi == null || Painel.nomebi == ""){
                Painel.nomebi = undefined;
            }
            Painel.url = document.getElementById('txtUrlPainelAlteracao').value;
            if(Painel.url == null || Painel.url == ""){
                Painel.url = undefined;
            }
            Painel.codusuario = codusuario[codusuario.selectedIndex].value;//Pega o valar do atributo value do select que no caso e o codusuario
            if(Painel.codusuario == 1000){
                Painel.codusuario = undefined;
            }
            Painel.ativo = true;
            JsonPainel = JSON.stringify(Painel);   
            
            request.open('PUT','http://192.168.0.53:8089/api/Bi/'+Painel.codbi, true);
            request.setRequestHeader('Content-type','application/json');
            request.setRequestHeader('Authorization','bearer '+token);
            request.send(JsonPainel);
            request.onreadystatechange = function(){
                if(request.readyState == request.DONE){
                    if(request.status == 200){
                        var NovoPainel = new Object();
                        NovoPainel = JSON.parse(request.response);
                        console.log(NovoPainel);
                        if(NovoPainel.status != false){
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                            divTextoErro.innerHTML = "Painel alterado com sucesso!";
                        }else{
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";                    
                            if(NovoPainel[0].errorInfo[0] == 23502){
                                divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                            }
                        }
                    }
                }
            }
        }else{
            divErro.className = "d-flex col-12 justify-content-center";
            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
            divTextoErro.innerHTML = "Por favor, selecione um painel para alterar";
            
        }
    }else{ //Desativacao
        if(Painel.codbi != 1000){
            Painel.ativo = false;
            JsonPainel = JSON.stringify(Painel); 
            request.open('PUT','http://192.168.0.53:8089/api/Bi/desativa/', true);
            request.setRequestHeader('Content-type','application/json');
            request.setRequestHeader('Authorization','bearer '+token);
            request.send(JsonPainel);
            request.onreadystatechange = function(){
                if(request.readyState == request.DONE){
                    if(request.status == 200){
                        Painel.status = JSON.parse(request.response);
                        if(Painel.status != false){
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                            divTextoErro.innerHTML = "Painel desativado com sucesso!";
                        }else{
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                            divTextoErro.innerHTML = "Por favor, selecione um painel para alterar";
                        }
                    }
                }
            }
        }else{
            divErro.className = "d-flex col-12 justify-content-center";
            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
            divTextoErro.innerHTML = "Por favor, selecione um painel para alterar";
        }
    }
    
    
}