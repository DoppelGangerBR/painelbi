function AbreTab(event, tab)    {
    var i, tabs, tabLinks;
    tabs = document.getElementsByClassName("conteudoTab");
    for (i = 0; i < tabs.length; i++){
        tabs[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("tablinks");
    for(i = 0; i < tabLinks.length; i++){
        tabLinks[i].className = tabLinks[i].className.replace("active", "");
    }
    document.getElementById(tab).style.display = "block";
    event.currentTarget.className += " active";
}
